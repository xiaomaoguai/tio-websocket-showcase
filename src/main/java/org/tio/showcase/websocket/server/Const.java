/**
 * 
 */
package org.tio.showcase.websocket.server;

/**
 * @author tanyaowu
 * @modify by huang lin
 */
public class Const {
	/**
	 * 用于群聊的group id
	 */
	public static final String GROUP_ID = "showcase-websocket";

	public static final String WS_MSG_TOPIC_CHANNEL = "WS_MSG_TOPIC_CHANNEL";

	public static final String WS_USER_PREFIX = "WS_USER_PREFIX:";

	/**
	 * 客户端和服务端的交互动作枚举
	 */
	public enum Action {
		/**
		 * 点对点消息请求
		 */
		P2P_MSG_REQ(3),
		/**
		 * 点对点消息响应
		 */
		P2P_MSG_RESP(33),
		/**
		 * 群组消息请求
		 */
		GROUP_MSG_REQ(4),
		/**
		 * 群组消息响应
		 */
		GROUP_MSG_RESP(44),
		/**
		 * 心跳信息qingqiu
		 */
		HEART_BEAT_REQ(9);


		private int action;
		Action(int action) {
			this.action = action;
		}
		public int val(){
			return this.action;
		}
	}
}
